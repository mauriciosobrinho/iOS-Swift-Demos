//
//  OpenCVWrapper.h
//  TestOpenCVSwift
//
//  Created by Mauricio Luiz Sobrinho on 24/04/17.
//  Copyright © 2017 Mauricio Luiz Sobrinho. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OpenCVWrapper : NSObject

//Function to get OpenCV Version
+(NSString *) openCVVersionString;

//Function to convert an image to grayscale
+(UIImage *) makeGrayFromImage:(UIImage *)image;


@end
