//
//  OpenCVWrapper.m
//  TestOpenCVSwift
//
//  Created by Mauricio Luiz Sobrinho on 24/04/17.
//  Copyright © 2017 Mauricio Luiz Sobrinho. All rights reserved.
//

#import "OpenCVWrapper.h"
#import <opencv2/opencv.hpp>
#import <opencv2/imgcodecs/ios.h>

@implementation OpenCVWrapper



//Here we can C++ code

+(NSString *)openCVVersionString{
    return [NSString stringWithFormat:@"OpenCV Version %s", CV_VERSION];
}

+(UIImage *) makeGrayFromImage:(UIImage *)image{
    //Transform UIImage to cv::Mat
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    
    //if the image was already grayscale, return it
    if (imageMat.channels() == 1) return image;
    
    //Transform the cv::Mat color image to gray
    cv::Mat grayMat;
    cv::cvtColor(imageMat, grayMat, CV_BGR2GRAY);
    
    //Transform grayMat to UIMat and return
    return MatToUIImage(grayMat);
}

@end
