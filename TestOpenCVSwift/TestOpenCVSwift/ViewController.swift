//
//  ViewController.swift
//  TestOpenCVSwift
//
//  Created by Mauricio Luiz Sobrinho on 24/04/17.
//  Copyright © 2017 Mauricio Luiz Sobrinho. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var OpenCVVersionLabel: UILabel!
    
    @IBOutlet weak var lenaImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        OpenCVVersionLabel.text = OpenCVWrapper.openCVVersionString()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func toGrayscaleButtonTouched(_ sender: Any) {
        lenaImageView.image = OpenCVWrapper.makeGray(from: lenaImageView.image)
    }

}

