//
//  ViewController.swift
//  Calculator
//
//  Created by Mauricio Luiz Sobrinho on 06/04/17.
//  Copyright © 2017 Mauricio Luiz Sobrinho. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet fileprivate weak var display: UILabel!
    
    @IBOutlet fileprivate weak var descriptionLabel: UILabel!

    fileprivate var userIsInTheMiddleOfTyping = false
    fileprivate let DECIMAL_CHAR = "."
    
    @IBAction fileprivate func touchDigit(_ sender: UIButton) {
        let digit = sender.currentTitle!
        
        if userIsInTheMiddleOfTyping {
            let textCurrentlyInDisplay = display.text!
            if digit != DECIMAL_CHAR || display.text!.range(of: DECIMAL_CHAR) == nil {
                display.text = textCurrentlyInDisplay + digit
            }
        } else {
            if digit == DECIMAL_CHAR {
                display.text = "0\(digit)"
            } else {
                display.text = digit
            }
        }
        
        userIsInTheMiddleOfTyping = true
    }
    
    fileprivate var displayValue: Double {
        get {
            return Double(display.text!)!
        }
        
        set {
            display.text = String(newValue)
        }
    }
    
    fileprivate var brain = CalculatorBrain()
    
    @IBAction fileprivate func performOperation(_ sender: UIButton) {
        if userIsInTheMiddleOfTyping {
            brain.setOperand(displayValue)
            userIsInTheMiddleOfTyping = false
        }
        
        if let mathOperation = sender.currentTitle {
            brain.performOperand(mathOperation)
        }
        
        displayValue = brain.result

        descriptionLabel.text = brain.description
    }
}

